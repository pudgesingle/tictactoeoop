/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tictactoeoop;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author PlugPC
 */
public class Game {
    
    private Scanner sc = new Scanner(System.in);
    private int col;
    private int row;
    Table table = null;
    Player o = null;
    Player x = null;
    
    public Game(){
        this.o = new Player('o');
        this.x = new Player('x');
    }
    public void run(){
        while(true){
            this.runOnce();
            if(askContinue()){
                return;
            }
        }
    }
    private int getRandomNumber(int min, int max){
        return (int)((Math.random() * (max-min))+min);
    }
    
    public void newGame(){
        if(getRandomNumber(1,100)%2==0){
            this.table = new Table(o, x);
        }
        else{
            this.table = new Table(x, o);
        }
        
    }
    
    public void runOnce(){
        this.showWelcome();
        this.newGame();
        while(true){
            this.showTable();
            this.showTurn();
            this.inputRowCol();
            if(table.checkWin()){
                showResult();
                this.showStat();
                return;
            }
            table.swithPlayer();
        }
        
    }

    private void showResult() {
        if(table.getWinner() != null){
            showWin();
        }
        else{
            showDraw();
        }
    }

    private void showDraw() {
        System.out.println("Draw");
    }

    private void showWin() {
        System.out.println(table.getWinner().getName()+" Win!!");
    }
    
    private void showWelcome(){
        System.out.println("Welcome to Tictactoe Game");
    }

    private void showTable() {
        char[][] data = this.table.getData();
        for(int row = 0;row < data.length;row++){
            System.out.print("| ");
            for(int col = 0;col < data[row].length;col++){
                System.out.print(" "+data[row][col]+" |");
            }
            System.out.println("");
        }
    }

    private void showTurn() {
        System.out.println(table.getCurrentPlayer().getName()+" Turn");
    }

    private void input() {
        while(true){
         try{
            System.out.print("Pls input row col: ");
            this.row = sc.nextInt();
            this.col = sc.nextInt();
            if(sc.hasNext()){
                System.out.println("just input row and col");
                continue;
            }
            return;
        }catch(InputMismatchException iE){
            sc.next();
            System.out.println("Error");
        } 
        }    
    }

    private void inputRowCol() {
        while(true){
           this.input();
           try{
             if(table.setRowCol(row, col)){
                return;
            }
           }catch(ArrayIndexOutOfBoundsException e){
               System.out.println("Error");
           }
 
        }
    }

    private boolean askContinue() {
        while(true){
            System.out.println("Continue: Y/n? ");
        String ans = sc.next();
        if(ans.equals("Y")){
            return false;
        }
        else if(ans.equals("n")){
            return true;
        }
        }
    }

    private void showStat() {
        System.out.println(o.getName()+"(win, lose, draw : "+o.getWin()+", "+o.getLose()+", "+o.getDraw()+" )");
        System.out.println(x.getName()+"(win, lose, draw : "+x.getWin()+", "+x.getLose()+", "+x.getDraw()+" )");
    }
}
